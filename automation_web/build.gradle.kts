plugins {
    java
    kotlin("jvm")
    id("io.qameta.allure") version "2.8.1"
}

group = "com.automation.web.verbet"
version = "1"

repositories {
    mavenCentral()
}

val testVersion = "7.4.0"
val allureVersion = "2.14.0"
val allureJavaAnnotationVersion = "1.5.4"
val junitVersion = "5.7.2"
val playRightVersion = "1.11.1"

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("stdlib-jdk8", "1.5.10"))

    implementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    implementation("org.junit.jupiter:junit-jupiter-engine")

    implementation("com.microsoft.playwright:playwright:$playRightVersion")
    implementation("com.microsoft.playwright:driver-bundle:$playRightVersion")
    implementation("org.slf4j:slf4j-simple:1.7.30")
    implementation("io.qameta.allure:allure-attachments:$allureVersion")
    implementation("io.qameta.allure:allure-rest-assured:$allureVersion")
    implementation("io.qameta.allure:allure-junit5:$allureVersion")
    implementation("org.hamcrest:hamcrest:2.2")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform {
    }
}

allure {
    version = allureVersion
    autoconfigure = true
    aspectjweaver = true
    allureJavaVersion = allureVersion
    resultsDir = file("$projectDir/allure-results")
    reportDir = file("$projectDir/allure-report")
    useJUnit5 {
        version = allureVersion
    }
}

tasks.register("cleanAllure"){
    delete(files("$projectDir/allure-results", "$projectDir/allure-report"))
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}