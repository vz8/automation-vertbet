package com.vertbet.web.core

import com.microsoft.playwright.Browser
import com.microsoft.playwright.BrowserType
import com.microsoft.playwright.Page
import com.microsoft.playwright.Playwright
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach

/***
 *  Current realisation need just for the integration with
 *  gitlab piplines. It would be re-write soon when product will be
 *  ready for the web automation.
 */


open class BaseTest {

    lateinit var page: Page
    lateinit var browser: Browser


    /**
     *  Before Each test will opened browser.
     *  Tests should have to be independent.
     *  After each test will closed.
     */
    @BeforeEach
    fun startTap() {
        val playwright = Playwright.create()
        browser = playwright.chromium().launch(BrowserType.LaunchOptions().setHeadless(false));
        page = browser.newPage()
        page.setViewportSize(1000, 1080)
        page.navigate("https://dev.vertbet.com/en/main")
    }

    @AfterEach
    fun afterAll() {
        page.close()
    }
}