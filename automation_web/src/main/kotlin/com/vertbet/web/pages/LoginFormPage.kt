package com.vertbet.web.pages

import com.microsoft.playwright.Page
import io.qameta.allure.Step

class LoginFormPage(private val page: Page) {

    private val emailField = "//input[@name='email']"
    private val passwordField = "//input[@name='password']"
    private val loginForm = "//section[@id='login']"
    private val submitButton = "//button[contains(@class, 'submit')]"


    @Step("Login Form is displayed on the page")
    fun loginFormIsReflectedOnThePage(): LoginFormPage {
        page.waitForSelector(loginForm)
        return this
    }

    @Step("Enter email {0} address")
    fun enterTheEmailAddress(email: String): LoginFormPage {
        page.fill(emailField, email)
        return this
    }

    @Step("Enter password {0} address")
    fun enterThePassword(password: String): LoginFormPage {
        page.fill(passwordField, password)
        return this
    }

    @Step("Click Log in ")
    fun submitLogin(): LoginFormPage {
        page.click(submitButton)
        return this
    }
}