package com.vertbet.web.pages

import com.microsoft.playwright.Page
import io.qameta.allure.Step
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat

class MainPage(private val page: Page) {
    private val buttonLogin = "//button[@aria-label='Login']"
    private val buttonSignUp = "//button[@aria-label='Sign Up']"
    private val username = "//span[@class='username']"


    @Step("Click Button Login On the Main Page")
    fun clickButtonLogin(): LoginFormPage {
        page.waitForSelector(buttonLogin)
        page.click(buttonLogin)
        return LoginFormPage(page)
    }

    @Step("UserName {0} is displayed On the Main Page ")
    fun verifyLoggedUserByName(nameUser: String): MainPage {
        page.waitForSelector(username)
        assertThat(page.querySelector(username).textContent().trim(), equalTo(nameUser))
        return this
    }

}