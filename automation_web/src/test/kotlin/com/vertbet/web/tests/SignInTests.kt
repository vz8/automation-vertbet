package com.vertbet.web.tests

import com.vertbet.web.core.BaseTest
import com.vertbet.web.pages.MainPage
import io.qameta.allure.Description
import org.junit.jupiter.api.*

/***
 *  Current realisation need just for the integration with
 *  gitlab piplines. It would be re-write soon when product will be
 *  ready for the web automation.
 */

@DisplayName("Sign In Tests")
class SignInTests: BaseTest() {

   lateinit var mainPage: MainPage

   @BeforeEach
   fun doBeForEachTest () {
      mainPage = MainPage(page)
   }

   @Test
   @Description("Positive")
   fun signInWithCorrectCredentials() {
      mainPage
         .clickButtonLogin()
         .loginFormIsReflectedOnThePage()
         .enterTheEmailAddress(email = "testgameuser@yopmail.com")
         .enterThePassword(password = "Dfg6#fdg!jkp9")
         .submitLogin()

      mainPage
         .verifyLoggedUserByName(nameUser = "TestGameUser")
   }

   @Test
   @Tag("Smoke")
   @Description("Negative")
   fun signInWithCworrectCredentials() {
      mainPage
         .clickButtonLogin()
         .loginFormIsReflectedOnThePage()
         .enterTheEmailAddress(email = "testgameuser@yopmail.com")
         .enterThePassword(password = "Dfg6#fdg!jkp9")
         .submitLogin()

      mainPage
         .verifyLoggedUserByName(nameUser = "TestGameUserNegative")
   }

}